package helper;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import pojo.BookModel;

@XmlRootElement
public class Bookstore {

	@XmlElementWrapper(name = "bookList")
	@XmlElement(name = "book")
	private ArrayList<BookModel> bookList;

	public void setBookList(ArrayList<BookModel> bookList) {
		this.bookList = bookList;
	}

	public ArrayList<BookModel> getBooksList() {
		return bookList;
	}

	public void addBook(BookModel book) {
		try {
			if (bookList == null) {
				bookList = new ArrayList<BookModel>();
			}
			bookList.add(book);

		} catch (Exception e) {
			System.out.println("Add book problem: "+e);
		}
	}

	public void removeBook(int id) {
		for (int i = 0; i < bookList.size(); i++) {
			int idNew = bookList.get(i).getIdBook();
			if (id == idNew) {
				bookList.remove(i);
			}
		}
	}
}