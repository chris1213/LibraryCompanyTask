package api;

import java.util.ArrayList;

import pojo.BookModel;

public interface LibraryAPI {
	void addBook(BookModel book);
	void removeBookByID(int id);
	void changeStatusBook(BookModel book);
	boolean checkBookStatus();
	ArrayList<BookModel> getBooks();
	BookModel getBookById(int id);
	BookModel getBook(int id);
}
