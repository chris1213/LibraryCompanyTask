package handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

public class SaveHandler{
	

	@Execute
	public void execute(Shell shell) {
		MessageDialog.openInformation(shell, "Save", "Application have been saved");
	}

}